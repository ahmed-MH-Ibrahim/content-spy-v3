import $ from "jquery";
import swal from 'sweetalert2';
import moment from "moment";
import  '../assets/css/content.css';
import "../assets/scanning-logo.gif";
import "./globals.js";
import { createApp } from 'vue';
import ContentSpyVue from '../view/contentspy.vue';


//Global variables and data use through out page's session
var url;
var currentPageType;
var isPageLoaded = false;
var vm;
var lastPostDate,prevLastDate;
var scrollInterval,executePostCheckInterval;
var prevDocumentHeight; //document height for current page
var noTitleFound = "No title assigned";

$(document).ready(function(){

	//handle extension events
	chrome.runtime.onMessage.addListener(function(request, sender, sendResponse)
	{
		if(request.command == "is-page-loaded")
		{
		  //get url every check in case user redirects without page refresh
		  url = window.location.href;
		  currentPageType = "none";
		  isPageLoaded = false;

		  checkCurrentPageType();

		  //Update popup with current page type and page loaded state
		  chrome.runtime.sendMessage({"command": "page-loaded", currentPageType:currentPageType,isPageLoaded:isPageLoaded});                  
		}
		else if(request.command == "start-scan")
		{
		  vm.isCanceled = false;
		  vm.resetData();

		  vm.firstDayPicked = moment(request.firstDayPicked);
		  vm.lastDayPicked = moment(request.lastDayPicked);
		  vm.totalDays = vm.lastDayPicked.diff(vm.firstDayPicked, 'days');
		  vm.currentDays = 0;
		  vm.totalDaysScrolled = 0;
		  vm.parsingPostsStarted = true;

		  //storing "Start parsing" state and needed data
		  //starting parsing flag and store state
		  vm.parsingPostsStarted = true;
		  chrome.storage.local.set({"parsingPostsStarted":true});
		  chrome.storage.local.set({"currentDays":vm.currentDays});
		  chrome.storage.local.set({"totalDaysScrolled":0});
		  chrome.storage.local.set({"totalDays":vm.totalDays});
		  chrome.storage.local.set({"lastDayPicked":vm.lastDayPicked});
		  chrome.storage.local.set({"firstDayPicked":vm.firstDayPicked});

		  //add window to dom to show script starting
		  vm.startWindowDom();

		  vm.scrollToDate();

		}
		else if(request.command == "cancel-scan")
		{
		  vm.cancelScan();
		}
	});

	//appending stylesheets
	$('body').append('<link rel="stylesheet" type="text/css" href="'+chrome.runtime.getURL('css/content.css')+'">');


	//append empty div element to attach vuejs to
	$("body").append("<div id='content-spy-container'></div>");

	//append translate popup vue to site dom
	vm = createApp(ContentSpyVue).mount('#content-spy-container');

	//set logo
	$("#content-spy-container #scan-logo").attr("src", chrome.runtime.getURL('img/scanning-logo.gif')).css("width","100px");
});


function checkCurrentPageType() {
    //if content loaded, start
    if(!$(".rq0escxv").length)
      return;

    if(url.includes("https://www.facebook.com/groups"))
    {
      currentPageType = 'group';
    }
    else
    {
      currentPageType = 'profile';

      if($("div[role='feed']").length == 0)
        vm.mainContainerDom = "div[role='main']";
    }

    console.log(currentPageType+" Found");

    isPageLoaded = true;
}


