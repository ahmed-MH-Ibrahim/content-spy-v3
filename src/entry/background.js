
//Handle extension events
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if(request.command == "open-post-link")
    {
        chrome.tabs.create({url: request.url, active: false});         
    }
    else if(request.command == "updateBadge")
    {
      updateNotificationsBadge(request.value);
    } 
    else if(request.command == "clearBadge")
    {
      clearNotificationsBadge();
    } 
    else if(request.command == "notifyUser")
    {
      callNotification();
    }    
});

function updateNotificationsBadge(value = 0){

    value = value == 0 ? ' ':value;
    chrome.action.setBadgeText ( { text: value.toString() } );
    chrome.action.setBadgeBackgroundColor({ color: [0,255,0, 255] });
}

function clearNotificationsBadge(){

    chrome.action.setBadgeText ( { text: '' } );
}

function callNotification(){
    chrome.notifications.create(
        "contentspy-complete"+Date.now(),
        {
          type: "basic",
          iconUrl: "../img/notify-icon.png",
          priority: 1,
          title: "Running task is completed",
          message: "You can now view available posts",
        },
        function () {}
    );
}





